from flask import Flask
from flask_cors import CORS
from .ussd.views import blueprint as ussd


def create_app():
    app = Flask(__name__)
    CORS(app)

    app.register_blueprint(ussd, url_prefix='/api')

    if __name__ == '__main__':
        app.run(debug=True)

    return app
