# -*- coding: utf-8 -*-

from flask import Blueprint, jsonify, request

blueprint = Blueprint('ussd', __name__)


@blueprint.route('/ussd', methods=['POST'])
def handle():
    try:
        data = request.get_json()
        print(data)

        type = data['Type']
        session_id = data['SessionId']
        message = data['Message']
        mobile = data['Mobile']
        sequence = data['Sequence']

        if type == 'Initiation':
            initiation_res = {
                'Type': 'Response',
                'DataType': 'select',
                'SessionId': session_id,
                'FieldType': 'text',
                'FieldName': '',
                'Label': 'Mustapha Ussif Campaign Donation',
                'Message': 'Mustapha Ussif for Yagaba-Kubori Constituency Campaign Donation. Select amount to donate\n1. GHS 100\n2. GHS 200\n3. GHS 500\n4. GHS 1000\n5. Other\n',
            }
            print(initiation_res)
            return jsonify(initiation_res)

        amount = 0
        if sequence == 2:
            if message == '1':
                amount = 100
            elif message == '2':
                amount = 200
            elif message == '3':
                amount = 500
            elif message == '4':
                amount = 1000
            elif message == '5':
                other_res = {
                    'Type': 'Response',
                    'DataType': 'input',
                    'SessionId': session_id,
                    'FieldType': 'number',
                    'Label': 'Mustapha Ussif Campaign Donation',
                    'Message': 'Enter amount you want to donate\n'
                }
                print(other_res)
                return jsonify(other_res)
        if sequence == 3:
            amount = int(message)

        default_res = {
            'Type': 'AddToCart',
            'SessionId': session_id,
            'DataType': 'display',
            'FieldType': 'text',
            'Message': 'You will receive a prompt soon\n',
            "Item": {
                "ItemName": "Donation of GHS" + str(amount) + " from " + str(mobile),
                "Qty": 1,
                "Price": amount
            }
        }
        print(default_res)
        return jsonify(default_res)
    except Exception as e:
        print(e)
        return jsonify({'message': 'error'})


@blueprint.route('/callback', methods=['POST'])
def callback():
    return jsonify({'paymentReceived': True})
