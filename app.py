
# -*- coding: utf-8 -*-
"""Create an application instance."""
from server.app import create_app

app = create_app()
